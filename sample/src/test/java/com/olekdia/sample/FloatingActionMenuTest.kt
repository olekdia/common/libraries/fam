package com.olekdia.sample

import android.content.Context
import android.os.Build
import android.view.View
import androidx.lifecycle.Lifecycle
import androidx.test.core.app.launchActivity
import androidx.test.platform.app.InstrumentationRegistry
import com.olekdia.fam.*
import com.olekdia.fam.R
import io.mockk.mockk
import io.mockk.verify
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import org.robolectric.annotation.LooperMode
import org.robolectric.shadows.ShadowLooper
import java.util.concurrent.TimeUnit

@LooperMode(LooperMode.Mode.PAUSED)
@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.O_MR1])
class FloatingActionMenuTest {

    private lateinit var context: Context

    private lateinit var onFamStateChangeListener: OnFamStateChangeListener
    private lateinit var onFabClickListener: OnFabClickListener
    private lateinit var onFabLongClickListener: OnFabLongClickListener

    @Before
    fun setup() {
        context = InstrumentationRegistry.getInstrumentation().context

        onFamStateChangeListener = mockk(relaxed = true, relaxUnitFun = true)
        onFabClickListener = mockk(relaxed = true, relaxUnitFun = true)
        onFabLongClickListener = mockk(relaxed = true, relaxUnitFun = true)
    }

    private fun waitUi() {
        ShadowLooper.runUiThreadTasks()
    }

    private fun waitAnimation(millis: Long) {
        Robolectric.getForegroundThreadScheduler().advanceBy(millis, TimeUnit.MILLISECONDS)
        Robolectric.flushForegroundThreadScheduler()
    }

//--------------------------------------------------------------------------------------------------
//  Tests
//--------------------------------------------------------------------------------------------------

    @Test
    fun `expand fam - scrim is added`() {
        launchActivity<SampleActivity>().moveToState(Lifecycle.State.RESUMED).let { scenario ->
            scenario.onActivity { activity ->
                val fam = activity.fam

                assertFalse(fam.isExpanded)
                fam.mainButton!!.performClick()
                waitUi()

                assertTrue(fam.isExpanded)
                activity.findViewById<View>(R.id.fab_menu_scrim).let { scrim ->
                    scrim!!.performClick()
                    waitUi()
                }
                assertFalse(fam.isExpanded)
                assertNull(activity.findViewById(R.id.fab_menu_scrim))
            }
        }
    }

    @Test
    fun `fam expanded, click on scrim - fam is collapsed`() {
        launchActivity<SampleActivity>().moveToState(Lifecycle.State.RESUMED).let { scenario ->
            scenario.onActivity { activity ->
                val fam = activity.fam

                assertFalse(fam.isExpanded)
                assertNull(activity.findViewById(R.id.fab_menu_scrim))

                fam.mainButton!!.performClick()
                waitUi()

                assertTrue(fam.isExpanded)
                assertNotNull(activity.findViewById(R.id.fab_menu_scrim))
            }
        }
    }

    @Test
    fun `expand fam, rotate - fam state is saved`() {
        launchActivity<SampleActivity>().moveToState(Lifecycle.State.RESUMED).let { scenario ->
            scenario.onActivity { activity ->
                val fam = activity.fam

                assertFalse(fam.isExpanded)
                assertNull(activity.findViewById(R.id.fab_menu_scrim))

                fam.mainButton!!.performClick()
                waitUi()

                assertTrue(fam.isExpanded)
                assertNotNull(activity.findViewById(R.id.fab_menu_scrim))
            }

            scenario.recreate()


            scenario.onActivity { activity ->
                val fam = activity.fam
                assertTrue(fam.isExpanded)
                assertNotNull(activity.findViewById(R.id.fab_menu_scrim))
            }
        }
    }

    @Test
    fun `fam with expandable false, click addButton - fam is hidden`() {
        launchActivity<SampleActivity>().moveToState(Lifecycle.State.RESUMED).let { scenario ->
            scenario.onActivity { activity ->
                val fam = activity.fam
                fam.isExpandable = false
                assertFalse(fam.isExpanded)

                fam.mainButton!!.performClick()
                waitUi()

                assertFalse(fam.isExpanded)
                assertNull(activity.findViewById(R.id.fab_menu_scrim))
                assertEquals(View.GONE, fam.mainButton!!.visibility)
            }
        }
    }

    @Test
    fun `fam with expandable false, long addButton - fam is hidden, long click handled`() {
        launchActivity<SampleActivity>().moveToState(Lifecycle.State.RESUMED).let { scenario ->
            scenario.onActivity { activity ->
                val fam = activity.fam
                fam.fabLongClickListener = onFabLongClickListener
                fam.isExpandable = false
                assertFalse(fam.isExpanded)

                fam.mainButton!!.performLongClick()
                waitUi()

                assertFalse(fam.isExpanded)
                assertNull(activity.findViewById(R.id.fab_menu_scrim))
                assertEquals(View.GONE, fam.mainButton!!.visibility)
                verify(exactly = 1) { onFabLongClickListener.onFabLongClick(com.olekdia.sample.R.id.fab_expand_menu_button) }
            }
        }
    }

    @Test
    fun `fam collapsed, long click addButton - fam remain collapsed, long click handled`() {
        launchActivity<SampleActivity>().moveToState(Lifecycle.State.RESUMED).let { scenario ->
            scenario.onActivity { activity ->
                val fam = activity.fam
                fam.fabLongClickListener = onFabLongClickListener
                fam.isExpandable = true
                assertFalse(fam.isExpanded)

                fam.mainButton!!.performLongClick()
                waitUi()

                assertFalse(fam.isExpanded)
                assertNull(activity.findViewById(R.id.fab_menu_scrim))
                verify(exactly = 1) { onFabLongClickListener.onFabLongClick(com.olekdia.sample.R.id.fab_expand_menu_button) }
            }
        }
    }

    @Test
    fun `fam expanded, long click addButton - fam is collapsed, long click handled`() {
        launchActivity<SampleActivity>().moveToState(Lifecycle.State.RESUMED).let { scenario ->
            scenario.onActivity { activity ->
                val fam = activity.fam
                fam.isExpandable = true
                fam.mainButton!!.performClick()

                fam.fabLongClickListener = onFabLongClickListener
                assertTrue(fam.isExpanded)

                fam.mainButton!!.performLongClick()
                waitUi()

                assertFalse(fam.isExpanded)
                assertNull(activity.findViewById(R.id.fab_menu_scrim))
                verify(exactly = 1) { onFabLongClickListener.onFabLongClick(com.olekdia.sample.R.id.fab_expand_menu_button) }
            }
        }
    }

    @Test
    fun `fam expanded, long click another button - fam is collapsed, long click handled`() {
        launchActivity<SampleActivity>().moveToState(Lifecycle.State.RESUMED).let { scenario ->
            scenario.onActivity { activity ->
                val fam = activity.fam
                fam.isExpandable = true
                fam.mainButton!!.performClick()

                fam.fabLongClickListener = onFabLongClickListener
                fam.fabClickListener = onFabClickListener
                assertTrue(fam.isExpanded)

                fam.getChildAt(0).let {
                    assertNotEquals(it, fam.mainButton)

                    it.performLongClick()
                    waitUi()

                    assertFalse(fam.isExpanded)
                    assertNull(activity.findViewById(R.id.fab_menu_scrim))
                    verify(exactly = 1) { onFabLongClickListener.onFabLongClick(it.id) }
                    verify(exactly = 0) { onFabClickListener.onFabClick(it.id) }
                }
            }
        }
    }

    @Test
    fun `fam expanded, click another button - fam is collapsed, click handled`() {
        launchActivity<SampleActivity>().moveToState(Lifecycle.State.RESUMED).let { scenario ->
            scenario.onActivity { activity ->
                val fam = activity.fam
                fam.isExpandable = true
                fam.mainButton!!.performClick()

                fam.fabLongClickListener = onFabLongClickListener
                fam.fabClickListener = onFabClickListener
                assertTrue(fam.isExpanded)

                fam.getChildAt(0).let {
                    assertNotEquals(it, fam.mainButton)

                    it.performClick()
                    waitUi()

                    assertFalse(fam.isExpanded)
                    assertNull(activity.findViewById(R.id.fab_menu_scrim))
                    verify(exactly = 1) { onFabClickListener.onFabClick(it.id) }
                    verify(exactly = 0) { onFabLongClickListener.onFabLongClick(any()) }
                }
            }
        }
    }
}