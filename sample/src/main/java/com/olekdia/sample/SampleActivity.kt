package com.olekdia.sample

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.olekdia.bottombar.BottomBar
import com.olekdia.fam.FloatingActionButton
import com.olekdia.fam.FloatingActionsMenu
import com.olekdia.fam.OnFabClickListener
import com.olekdia.fam.OnFabLongClickListener

class SampleActivity : AppCompatActivity(),
    OnFabClickListener,
    OnFabLongClickListener {

    lateinit var fam: FloatingActionsMenu
    private lateinit var mainContainer: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_sample)

        findViewById<View>(R.id.snack_btn).setOnClickListener { v: View? ->
            val snackbar = Snackbar
                .make(mainContainer, "Some action", Snackbar.LENGTH_LONG)
                .setAction(R.string.undo, null)
                .apply {
                    anchorView = fam.mainButton
                }

            snackbar.show()
        }

        mainContainer = findViewById(R.id.main_container)
        (findViewById<View>(R.id.bottom_bar) as? BottomBar)?.setItems(R.xml.bb_tabs)

        fam = findViewById(R.id.fab_menu)
        fam.apply {
            fabClickListener = this@SampleActivity
            fabLongClickListener = this@SampleActivity
        }
    }

    override fun onFabClick(btnId: Int) {
        val toastText: String = when (btnId) {
            R.id.fab_new_category -> "Category click"
            R.id.fab_new_act -> "Activity click"
            R.id.fab_new_reminder -> "Reminder click"
            R.id.fab_new_estimation -> "Estimation click"
            else -> "+ click"
        }

        val snackbar = Snackbar
            .make(mainContainer, toastText, Snackbar.LENGTH_LONG)
            .setAction(R.string.undo) { }
            .apply {
                anchorView = fam.mainButton
            }

        snackbar.show()
    }

    override fun onFabLongClick(btnId: Int) {
        val toastText: String = when (btnId) {
            R.id.fab_new_category -> "Category long click"
            R.id.fab_new_act -> "Activity long click"
            R.id.fab_new_reminder -> "Reminder long click"
            R.id.fab_new_estimation -> "Estimation long click"
            else -> "+ long click"
        }

        val toast = Toast.makeText(this, toastText, Toast.LENGTH_SHORT)
        toast.show()
    }
}