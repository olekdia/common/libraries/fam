package com.olekdia.fam

import android.animation.*
import android.animation.ValueAnimator.AnimatorUpdateListener
import android.annotation.SuppressLint
import android.content.Context
import android.content.res.TypedArray
import android.graphics.Matrix
import android.graphics.Rect
import android.os.Build
import android.os.Parcel
import android.os.Parcelable
import android.util.AttributeSet
import android.view.*
import android.view.View.OnClickListener
import android.view.View.OnLongClickListener
import android.view.animation.DecelerateInterpolator
import android.view.animation.Interpolator
import android.view.animation.OvershootInterpolator
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.PointerIconCompat
import com.olekdia.androidcommon.NO_RESOURCE
import com.olekdia.androidcommon.extensions.setTextAppearanceCompat
import com.olekdia.common.extensions.ifNotNull
import com.olekdia.common.extensions.toBoolean
import com.olekdia.common.extensions.toInt
import kotlin.math.max
import kotlin.math.min

//@CoordinatorLayout.DefaultBehavior(FamBehavior.class)
class FloatingActionsMenu @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = NO_RESOURCE
) : ViewGroup(context, attrs, defStyleAttr) {

//--------------------------------------------------------------------------------------------------
//  Members & constructors
//--------------------------------------------------------------------------------------------------

    private var roundingOffset = resources.getDimensionPixelSize(R.dimen.fab_rounding_offset)

    var expandDirection = 0

    var isExpandable = false
        set(isExpandable) {
            field = isExpandable
            if (!isExpandable) {
                mainButton?.let { button ->
                    button.imageMatrix?.let { matrix ->
                        matrix.reset()
                        button.imageMatrix = matrix
                    }
                }
            }
        }

    var isExpanded = false

    private var buttonSpacing = resources.getDimensionPixelSize(R.dimen.fab_actions_spacing) -
        resources.getDimensionPixelSize(R.dimen.fab_actions_spacing_hack_diff)
    private var maxButtonWidth = 0
    private var maxButtonHeight = 0
    private var buttonsCount = 0

    private var labelsMargin = resources.getDimensionPixelSize(R.dimen.fab_labels_margin)
    private var labelsVerticalOffset = resources.getDimensionPixelSize(R.dimen.fab_shadow_offset)
    private var labelsStyle = 0
    var labelsPosition = 0

    private val expandAnimation = AnimatorSet().setDuration(ANIMATION_DURATION)
    private val collapseAnimation = AnimatorSet().setDuration(ANIMATION_DURATION)

    var expandedDrawableRes = NO_RESOURCE
        set(res) {
            field = res
            if (isExpanded) {
                mainButton?.setImageResource(res)
            }
        }
    var collapsedDrawableRes = NO_RESOURCE
        set(res) {
            field = res
            if (!isExpanded) {
                mainButton?.setImageResource(res)
            }
        }

    var mainButton: FloatingActionButton? = null
        private set(button) {
            field = button

            if (button != null) {
                button.setOnClickListener(mainButtonClickListener)
                button.isLongClickable = true
                button.setOnLongClickListener(buttonLongClickListener)

                //addView(button, super.generateDefaultLayoutParams())

                button.scaleType = ImageView.ScaleType.MATRIX // Required

                bringChildToFront(button)

                if (expandedDrawableRes != NO_RESOURCE && isExpanded) {
                    button.setImageResource(expandedDrawableRes)
                }
                if (collapsedDrawableRes != NO_RESOURCE && !isExpanded) {
                    button.setImageResource(collapsedDrawableRes)
                }
            }
        }

    private var scrimView: View

    private var touchDelegateGroup: TouchDelegateGroup = TouchDelegateGroup(this)

    var famStateChangeListener: OnFamStateChangeListener? = null
    var fabClickListener: OnFabClickListener? = null
    var fabLongClickListener: OnFabLongClickListener? = null

    private val buttonLongClickListener = OnLongClickListener { v ->
        if (fabLongClickListener != null) {
            fabLongClickListener?.onFabLongClick(v.id)

            if (!isExpandable) {
                hide()
            } else if (isExpanded) {
                collapse()
                v.isPressed = false // Fix, without it fab become unclickable once
            }

            true
        } else {
            false
        }
    }

    private val mainButtonClickListener = OnClickListener { v ->
        if (!isExpandable) {
            hide()
            fabClickListener?.onFabClick(v.id)
        } else {
            if (isExpanded) {
                fabClickListener?.onFabClick(v.id)
            }
            toggle()
        }
    }

    private val innerButtonClickListener = OnClickListener { v ->
        collapse()
        fabClickListener?.onFabClick(v.id)
    }

//--------------------------------------------------------------------------------------------------
//  Create methods
//--------------------------------------------------------------------------------------------------

    init {
        touchDelegate = touchDelegateGroup

        val a: TypedArray = context.obtainStyledAttributes(
            attrs,
            R.styleable.FloatingActionsMenu,
            defStyleAttr,
            NO_RESOURCE
        )

        val inflater = LayoutInflater.from(context)

        expandDirection = a.getInt(
            R.styleable.FloatingActionsMenu_fab_expandDirection,
            resources.getInteger(R.integer.fab_expand_direction)
        )

        labelsStyle = a.getResourceId(
            R.styleable.FloatingActionsMenu_fab_labelStyle,
            R.style.FabMenuLabels
        )
        labelsPosition = a.getInt(
            R.styleable.FloatingActionsMenu_fab_labelsPosition,
            LABELS_ON_LEFT_SIDE
        )

        // Scrim
        scrimView = inflater.inflate(R.layout.scrim_fab, this, false)
        scrimView.setOnClickListener {
            collapse()
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            scrimView.elevation = elevation - 1
        }

        initAnimations()

        expandedDrawableRes = a.getResourceId(
            R.styleable.FloatingActionsMenu_fab_expandedDrawable,
            NO_RESOURCE
        )
        collapsedDrawableRes = a.getResourceId(
            R.styleable.FloatingActionsMenu_fab_collapsedDrawable,
            NO_RESOURCE
        )

        isExpandable = a.getBoolean(
            R.styleable.FloatingActionsMenu_fab_isExpandable,
            true
        )

        a.recycle()
    }

    private fun initAnimations() {
        val mainButtonExpandAnim: ValueAnimator = ValueAnimator.ofFloat(0F, 1F)
//        addExpandAnim.interpolator = VisibilityDelegate.FAST_OUT_SLOW_IN_INTERPOLATOR

        mainButtonExpandAnim.addUpdateListener(object : AnimatorUpdateListener {
            var flag: Boolean = true
            private var centerX: Int = 0
            private var centerY: Int = 0

            override fun onAnimationUpdate(animation: ValueAnimator) {
                val animatedValue: Float = animation.animatedValue as? Float ?: return
                mainButton?.apply {
                    ifNotNull(
                        this.drawable?.bounds, this.imageMatrix
                    ) { rect: Rect, matrix: Matrix ->
                        centerX = rect.centerX()
                        centerY = rect.centerY()

                        if (animatedValue < 0.5F) {
                            flag = true
                            val scale: Float = 1F - animatedValue * .7F / .5F

                            matrix.setScale(
                                scale,
                                scale,
                                centerX.toFloat(),
                                centerY.toFloat()
                            )
                            matrix.postRotate(
                                animatedValue * 90 / .5F - 180,
                                centerX.toFloat(),
                                centerY.toFloat()
                            )
                        } else {
                            if (flag) {
                                this.setImageResource(expandedDrawableRes)
                                flag = false
                            }
                            val scale: Float = .3F + (animatedValue - .5F) * .7F / .5F

                            matrix.setScale(
                                scale,
                                scale,
                                centerX.toFloat(),
                                centerY.toFloat()
                            )
                            matrix.postRotate(
                                animatedValue * 90 / .5F - 180,
                                centerX.toFloat(),
                                centerY.toFloat()
                            )
                        }
                        this.imageMatrix = matrix
                    }

                    this.invalidate()
                }
            }
        })

        mainButtonExpandAnim.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationStart(animator: Animator) {
                mainButton?.apply {
                    this.setImageResource(collapsedDrawableRes)

                    ifNotNull(
                        this.drawable?.bounds, this.imageMatrix
                    ) { rect, matrix ->
                        matrix.reset()
                        matrix.setScale(
                            1F,
                            1F,
                            rect.centerX().toFloat(),
                            rect.centerY().toFloat()
                        )
                        matrix.postRotate(
                            -180F,
                            rect.centerX().toFloat(),
                            rect.centerY().toFloat()
                        )

                        this.imageMatrix = matrix
                    }
                    this.invalidate()
                }
            }
        })

        val mainButtonCollapseAnim = ValueAnimator.ofFloat(0F, 1F)
//        addCollapseAnim.interpolator = LinearInterpolator()

        mainButtonCollapseAnim.addUpdateListener(object : AnimatorUpdateListener {
            private var flag: Boolean = true
            private var centerX: Int = 0
            private var centerY: Int = 0

            override fun onAnimationUpdate(animation: ValueAnimator) {
                val animatedValue: Float = animation.animatedValue as? Float ?: return
                mainButton?.apply {
                    ifNotNull(
                        this.drawable?.bounds, this.imageMatrix
                    ) { rect: Rect, matrix: Matrix ->
                        centerX = rect.centerX()
                        centerY = rect.centerY()

                        if (animatedValue < 0.5F) {
                            flag = true
                            val scale: Float = 1F - animatedValue * .7F / .5F

                            matrix.setScale(
                                scale,
                                scale,
                                centerX.toFloat(),
                                centerY.toFloat()
                            )
                            matrix.postRotate(
                                animatedValue * 90 / .5F,
                                centerX.toFloat(),
                                centerY.toFloat()
                            )
                        } else {
                            if (flag) {
                                this.setImageResource(collapsedDrawableRes)
                                flag = false
                            }
                            val scale: Float = .3F + (animatedValue - .5F) * .7F / .5F

                            matrix.setScale(
                                scale,
                                scale,
                                centerX.toFloat(),
                                centerY.toFloat()
                            )
                            matrix.postRotate(
                                animatedValue * 90 / .5F + 90,
                                centerX.toFloat(),
                                centerY.toFloat()
                            )
                        }
                        this.imageMatrix = matrix
                    }

                    this.invalidate()
                }
            }
        })

        mainButtonCollapseAnim.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationStart(animator: Animator) {
                mainButton?.apply {
                    this.setImageResource(expandedDrawableRes)

                    ifNotNull(
                        this.drawable?.bounds, this.imageMatrix
                    ) { rect: Rect, matrix: Matrix ->

                        matrix.reset()
                        matrix.setScale(
                            1F,
                            1F,
                            rect.centerX().toFloat(),
                            rect.centerY().toFloat()
                        )
                        matrix.postRotate(
                            0F,
                            rect.centerX().toFloat(),
                            rect.centerY().toFloat()
                        )
                        this.imageMatrix = matrix
                    }

                    this.invalidate()
                }
            }
        })

        expandAnimation.play(mainButtonExpandAnim)
        collapseAnimation.play(mainButtonCollapseAnim)

        val scrimExpandAnim: ValueAnimator = ValueAnimator.ofInt(0, SCRIM_ALPHA)
        scrimExpandAnim.addUpdateListener { animation: ValueAnimator ->
            scrimView.background.alpha = animation.animatedValue as? Int ?: 0
        }

        expandAnimation.play(scrimExpandAnim)
    }

    private fun createLabels() {
        val context: Context = ContextThemeWrapper(context, labelsStyle)

        childCount.let { childrenCount ->
            if (childrenCount > buttonsCount) {
                removeViews(buttonsCount, childrenCount - buttonsCount)
            }
        }

        for (i in 0 until buttonsCount) {
            val button: FloatingActionButton = getChildAt(i) as? FloatingActionButton ?: continue
            val title: String = button.title ?: continue

            val label: TextView = button.getTag(R.id.fab_label)
                as? TextView
                ?: TextView(context)
                    .apply {
                        setTextAppearanceCompat(labelsStyle)
                        text = title
                        isFocusable = false
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            pointerIcon = PointerIcon.getSystemIcon(
                                context,
                                PointerIconCompat.TYPE_HAND
                            )
                        }
                    }

            addView(label)
            button.setTag(R.id.fab_label, label)
        }
    }

//--------------------------------------------------------------------------------------------------
//  Getters & setters
//--------------------------------------------------------------------------------------------------

    fun setExpandedMainTitle(title: String?) {
        mainButton?.title = title
    }

    fun addButton(button: FloatingActionButton) {
        addView(button, buttonsCount - 1)
        buttonsCount++
        if (labelsStyle != 0) {
            createLabels()
        }
    }

    fun removeButton(button: FloatingActionButton) {
        removeView(button.labelView)
        removeView(button)
        button.setTag(R.id.fab_label, null)
        buttonsCount--
    }

    @SuppressLint("RestrictedApi")
    fun hideButtons(vararg toHideIndexes: Int) {
        if (toHideIndexes.isEmpty()) return

        for (i in 0 until childCount) {
            val v: View? = getChildAt(i)

            if (v is FloatingActionButton && v != mainButton) {
                val button : FloatingActionButton = v

                if (toHideIndexes.contains(i)) {
                    if (button.visibility != View.GONE) {
                        button.apply {
                            visibility = View.GONE
                            labelView?.visibility = View.GONE
                        }
                    }
                } else {
                    if (button.visibility != View.VISIBLE) {
                        button.apply {
                            visibility = View.VISIBLE
                            labelView?.visibility = View.VISIBLE
                        }
                    }
                }
            } else {
                break
            }
        }

        touchDelegateGroup.clearTouchDelegates()
        requestLayout()
    }

//--------------------------------------------------------------------------------------------------
//  View override methods
//--------------------------------------------------------------------------------------------------

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        measureChildren(widthMeasureSpec, heightMeasureSpec)

        var width = 0
        var height = 0

        maxButtonWidth = 0
        maxButtonHeight = 0

        var maxLabelWidth = 0
        var buttonsVisible = 0

        for (i in 0 until buttonsCount) {
            val child: View = getChildAt(i)
            if (child.visibility == View.GONE) {
                continue
            }

            buttonsVisible++

            when (expandDirection) {
                EXPAND_UP,
                EXPAND_DOWN -> {
                    maxButtonWidth = max(maxButtonWidth, child.measuredWidth)
                    height += child.measuredHeight
                }

                EXPAND_LEFT,
                EXPAND_RIGHT -> {
                    width += child.measuredWidth
                    maxButtonHeight = max(maxButtonHeight, child.measuredHeight)
                }
            }

            if (!expandsHorizontally()) {
                val label: TextView? = child.getTag(R.id.fab_label) as? TextView
                label?.let {
                    maxLabelWidth = max(maxLabelWidth, it.measuredWidth)
                }
            }
        }

        if (expandsHorizontally()) {
            height = maxButtonHeight
        } else {
            width = maxButtonWidth + if (maxLabelWidth > 0) maxLabelWidth + labelsMargin else 0
        }

        when (expandDirection) {
            EXPAND_UP,
            EXPAND_DOWN -> {
                height += buttonSpacing * (buttonsVisible - 1)
                height = adjustForOvershoot(height)
            }

            EXPAND_LEFT,
            EXPAND_RIGHT -> {
                width += buttonSpacing * (buttonsVisible - 1)
                width = adjustForOvershoot(width)
            }
        }

        setMeasuredDimension(
            width + paddingRight + paddingLeft + roundingOffset,
            height + paddingBottom + roundingOffset
        )
    }

    private fun adjustForOvershoot(dimension: Int): Int = dimension * 12 / 10

    @SuppressLint("DrawAllocation")
    override fun onLayout(
        changed: Boolean, l: Int, t: Int, r: Int, b: Int
    ) {
        when (expandDirection) {
            EXPAND_UP,
            EXPAND_DOWN -> {
                val button: FloatingActionButton = mainButton ?: return

                val expandUp: Boolean = expandDirection == EXPAND_UP
                if (changed) {
                    touchDelegateGroup.clearTouchDelegates()
                }

                val mainButtonY: Int =
                    if (expandUp) b - t - button.measuredHeight - paddingBottom else 0
                // Ensure mainButton is centered on the line where the buttons should be
                val buttonsHorizontalCenter =
                    if (labelsPosition == LABELS_ON_LEFT_SIDE) {
                        r - l - maxButtonWidth / 2 - paddingRight
                    } else {
                        l + maxButtonWidth / 2 + paddingLeft
                    }
                val mainButtonLeft: Int = buttonsHorizontalCenter - button.measuredWidth / 2

                button.layout(
                    mainButtonLeft,
                    mainButtonY,
                    mainButtonLeft + button.measuredWidth,
                    mainButtonY + button.measuredHeight
                )

                val labelsOffset: Int = maxButtonWidth / 2 + labelsMargin * 2
                val labelsXNearButton: Int =
                    if (labelsPosition == LABELS_ON_LEFT_SIDE) {
                        buttonsHorizontalCenter - labelsOffset
                    } else {
                        buttonsHorizontalCenter + labelsOffset
                    }
                var nextY: Int =
                    if (expandUp) {
                        mainButtonY - buttonSpacing
                    } else {
                        mainButtonY + button.measuredHeight + buttonSpacing
                    }

                var i: Int = buttonsCount - 1
                while (i >= 0) {
                    val child: View = getChildAt(i)

                    if (child.visibility == View.GONE) {
                        i--
                        continue
                    }

                    val expandedTranslation = 0F

                    if (child === mainButton) { // Main fab only for this directions
                        if (expandDirection != EXPAND_UP) {
                            i--
                            continue
                        }

                        val label: View = child.getTag(R.id.fab_label) as? View ?: return

                        val labelXAwayFromButton: Int =
                            if (labelsPosition == LABELS_ON_LEFT_SIDE) {
                                labelsXNearButton - label.measuredWidth
                            } else {
                                labelsXNearButton + label.measuredWidth
                            }
                        val labelLeft: Int =
                            if (labelsPosition == LABELS_ON_LEFT_SIDE) {
                                labelXAwayFromButton
                            } else {
                                labelsXNearButton
                            }
                        val labelRight: Int =
                            if (labelsPosition == LABELS_ON_LEFT_SIDE) {
                                labelsXNearButton
                            } else {
                                labelXAwayFromButton
                            }
                        val labelTop: Int = mainButtonY - labelsVerticalOffset +
                            (child.measuredHeight - label.measuredHeight) / 2

                        label.layout(
                            labelLeft,
                            labelTop,
                            labelRight,
                            labelTop +
                                label.measuredHeight
                        )

                        val collapsedAddTranslation: Float = button.measuredHeight / 3F
                        label.apply {
                            translationY =
                                if (isExpanded) expandedTranslation else collapsedAddTranslation
                            alpha = if (isExpanded) 1F else 0F
                        }

                        touchDelegateGroup.addTouchDelegate(
                            TouchDelegate(
                                Rect(
                                    min(mainButtonLeft, labelLeft),
                                    mainButtonY - buttonSpacing / 2,
                                    max(mainButtonLeft + child.measuredWidth, labelRight),
                                    mainButtonY + child.measuredHeight + buttonSpacing / 2
                                ),
                                child
                            )
                        )

                        val labelParams: LayoutParams =
                            label.layoutParams as? LayoutParams ?: return
                        labelParams.apply {
                            collapseDir.setFloatValues(
                                expandedTranslation,
                                collapsedAddTranslation
                            )
                            expandDir.setFloatValues(
                                collapsedAddTranslation,
                                expandedTranslation
                            )
                            setAnimationsTarget(label)
                        }

                        i--
                        continue
                    }

                    val childX: Int = buttonsHorizontalCenter - child.measuredWidth / 2
                    val childY: Int = if (expandUp) nextY - child.measuredHeight else nextY
                    child.layout(
                        childX,
                        childY,
                        childX + child.measuredWidth,
                        childY + child.measuredHeight
                    )

                    val collapsedTranslation: Float = mainButtonY - childY.toFloat()
                    child.apply {
                        translationY = if (isExpanded) expandedTranslation else collapsedTranslation
                        alpha = if (isExpanded) 1F else 0F
                    }

                    val params: LayoutParams = child.layoutParams as? LayoutParams ?: return
                    params.apply {
                        collapseDir.setFloatValues(expandedTranslation, collapsedTranslation)
                        expandDir.setFloatValues(collapsedTranslation, expandedTranslation)
                        setAnimationsTarget(child)
                    }

                    val label: View? = child.getTag(R.id.fab_label) as? View
                    if (label != null) {
                        val labelXAwayFromButton: Int =
                            if (labelsPosition == LABELS_ON_LEFT_SIDE) {
                                labelsXNearButton - label.measuredWidth
                            } else {
                                labelsXNearButton + label.measuredWidth
                            }
                        val labelLeft: Int =
                            if (labelsPosition == LABELS_ON_LEFT_SIDE) {
                                labelXAwayFromButton
                            } else {
                                labelsXNearButton
                            }
                        val labelRight: Int =
                            if (labelsPosition == LABELS_ON_LEFT_SIDE) {
                                labelsXNearButton
                            } else {
                                labelXAwayFromButton
                            }
                        val labelTop: Int = childY - labelsVerticalOffset +
                            (child.measuredHeight - label.measuredHeight) / 2

                        label.layout(
                            labelLeft,
                            labelTop,
                            labelRight,
                            labelTop + label.measuredHeight
                        )

                        touchDelegateGroup.addTouchDelegate(
                            TouchDelegate(
                                Rect(
                                    min(childX, labelLeft),
                                    childY - buttonSpacing / 2,
                                    max(childX + child.measuredWidth, labelRight),
                                    childY + child.measuredHeight + buttonSpacing / 2
                                ),
                                child
                            )
                        )

                        label.apply {
                            translationY =
                                if (isExpanded) expandedTranslation else collapsedTranslation
                            alpha = if (isExpanded) 1F else 0F
                        }

                        val labelParams: LayoutParams =
                            label.layoutParams as? LayoutParams ?: return
                        labelParams.apply {
                            collapseDir.setFloatValues(expandedTranslation, collapsedTranslation)
                            expandDir.setFloatValues(collapsedTranslation, expandedTranslation)
                            setAnimationsTarget(label)
                        }
                    }

                    nextY = if (expandUp) {
                        childY - buttonSpacing
                    } else {
                        childY + child.measuredHeight + buttonSpacing
                    }
                    i--
                }
            }

            EXPAND_LEFT,
            EXPAND_RIGHT -> {
                val button: FloatingActionButton = mainButton ?: return

                val expandLeft: Boolean = expandDirection == EXPAND_LEFT
                val mainButtonX: Int =
                    if (expandLeft) r - l - button.measuredWidth - paddingRight else paddingLeft
                // Ensure mainButton is centered on the line where the buttons should be
                val mainButtonTop: Int = b - t - maxButtonHeight +
                    (maxButtonHeight - button.measuredHeight) / 2 - paddingBottom

                button.layout(
                    mainButtonX,
                    mainButtonTop,
                    mainButtonX + button.measuredWidth,
                    mainButtonTop + button.measuredHeight
                )

                var nextX: Int =
                    if (expandLeft) {
                        mainButtonX - buttonSpacing
                    } else {
                        mainButtonX + button.measuredWidth + buttonSpacing
                    }

                var i: Int = buttonsCount - 1
                while (i >= 0) {
                    val child: View = getChildAt(i)

                    if (child === mainButton || child.visibility == View.GONE) {
                        i--
                        continue
                    }

                    val childX: Int = if (expandLeft) nextX - child.measuredWidth else nextX
                    val childY: Int =
                        mainButtonTop + (button.measuredHeight - child.measuredHeight) / 2

                    child.layout(
                        childX,
                        childY,
                        childX + child.measuredWidth,
                        childY + child.measuredHeight
                    )

                    val collapsedTranslation: Float = mainButtonX - childX.toFloat()
                    val expandedTranslation = 0F
                    child.apply {
                        translationX = if (isExpanded) expandedTranslation else collapsedTranslation
                        alpha = if (isExpanded) 1F else 0F
                    }

                    val params: LayoutParams = child.layoutParams as? LayoutParams ?: return
                    params.apply {
                        collapseDir.setFloatValues(expandedTranslation, collapsedTranslation)
                        expandDir.setFloatValues(collapsedTranslation, expandedTranslation)
                        setAnimationsTarget(child)
                    }

                    nextX = if (expandLeft) {
                        childX - buttonSpacing
                    } else {
                        childX + child.measuredWidth + buttonSpacing
                    }
                    i--
                }
            }
        }
    }

    override fun generateDefaultLayoutParams(): ViewGroup.LayoutParams =
        LayoutParams(super.generateDefaultLayoutParams())

    override fun generateLayoutParams(attrs: AttributeSet): ViewGroup.LayoutParams =
        LayoutParams(super.generateLayoutParams(attrs))

    override fun generateLayoutParams(p: ViewGroup.LayoutParams): ViewGroup.LayoutParams =
        LayoutParams(super.generateLayoutParams(p))

    private inner class LayoutParams(
        source: ViewGroup.LayoutParams?
    ) : ViewGroup.LayoutParams(source) {

        val expandDir: ObjectAnimator = ObjectAnimator()
        private val expandAlpha: ObjectAnimator = ObjectAnimator()
        val collapseDir: ObjectAnimator = ObjectAnimator()
        private val collapseAlpha: ObjectAnimator = ObjectAnimator()
        private var animationsSetToPlay = false

        fun setAnimationsTarget(view: View) {
            collapseAlpha.target = view
            collapseDir.target = view
            expandAlpha.target = view
            expandDir.target = view

            // Now that the animations have targets, set them to be played
            if (!animationsSetToPlay) {
                addLayerTypeListener(expandDir, view)
                addLayerTypeListener(collapseDir, view)

                collapseAnimation.run {
                    play(collapseAlpha)
                    play(collapseDir)
                }

                expandAnimation.run {
                    play(expandAlpha)
                    play(expandDir)
                }

                animationsSetToPlay = true
            }
        }

        private fun addLayerTypeListener(animator: Animator, view: View) {
            animator.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    view.setLayerType(View.LAYER_TYPE_NONE, null)
                }

                override fun onAnimationStart(animation: Animator) {
                    view.setLayerType(View.LAYER_TYPE_HARDWARE, null)
                }
            })
        }

        init {
            expandDir.interpolator = expandInterpolator
            collapseDir.interpolator = collapseInterpolator

            expandAlpha.apply {
                interpolator = alphaExpandInterpolator
                setProperty(View.ALPHA)
                setFloatValues(0F, 1F)
            }
            collapseAlpha.apply {
                interpolator = collapseInterpolator
                setProperty(View.ALPHA)
                setFloatValues(1F, 0F)
            }

            when (expandDirection) {
                EXPAND_UP,
                EXPAND_DOWN -> {
                    collapseDir.setProperty(View.TRANSLATION_Y)
                    expandDir.setProperty(View.TRANSLATION_Y)
                }

                EXPAND_LEFT,
                EXPAND_RIGHT -> {
                    collapseDir.setProperty(View.TRANSLATION_X)
                    expandDir.setProperty(View.TRANSLATION_X)
                }
            }
        }
    }

    override fun onFinishInflate() {
        super.onFinishInflate()

        mainButton = getChildAt(childCount - 1) as? FloatingActionButton
        buttonsCount = childCount

        if (labelsStyle != 0) {
            createLabels()
        }
    }

//--------------------------------------------------------------------------------------------------
//  Expand collapse methods
//--------------------------------------------------------------------------------------------------

    private fun expandsHorizontally(): Boolean =
        expandDirection == EXPAND_LEFT || expandDirection == EXPAND_RIGHT

    @JvmOverloads
    fun collapse(isImmediately: Boolean = false) {
        if (isExpanded) {
            isExpanded = false

            famStateChangeListener?.onFamStateChange(isExpanded = false)
            touchDelegateGroup.isEnabled = false

            collapseAnimation.apply {
                duration = if (isImmediately) 0 else ANIMATION_DURATION
                start()
            }
            expandAnimation.cancel()

            setChildrenClickable(false)
            removeScrim()
        }
    }

    fun toggle() {
        if (isExpanded) {
            collapse()
        } else {
            expand()
        }
    }

    @JvmOverloads
    fun expand(isImmediately: Boolean = false) {
        if (!isExpanded) {
            isExpanded = true

            famStateChangeListener?.onFamStateChange(isExpanded = true)

            addScrim()

            touchDelegateGroup.isEnabled = true

            collapseAnimation.cancel()
            expandAnimation.apply {
                duration = if (isImmediately) 0 else ANIMATION_DURATION
                start()
            }

            setChildrenClickable(true)
        }
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        mainButton?.isEnabled = enabled
    }

    private fun setChildrenClickable(isClickable: Boolean) {
        val clickListener: OnClickListener? = if (isClickable) innerButtonClickListener else null
        val longClickListener: OnLongClickListener? = if (isClickable) buttonLongClickListener else null

        for (i in 0 until buttonsCount) {
            val view: View? = getChildAt(i)

            if (view is FloatingActionButton && view !== mainButton) {
                view.apply {
                    setOnClickListener(clickListener)
                    setClickable(isClickable)
                    setOnLongClickListener(longClickListener)
                    isLongClickable = isClickable
                }
            }
        }

        touchDelegateGroup.isEnabled = isClickable
    }

    private fun addScrim() {
        (parent as? ViewGroup)?.addView(scrimView)
        bringToFront()
    }

    private fun removeScrim() {
        (parent as? ViewGroup)?.removeView(scrimView)
    }

//--------------------------------------------------------------------------------------------------
//  Show hide methods
//--------------------------------------------------------------------------------------------------

    @JvmOverloads
    fun show(isImmediately: Boolean = false) {
        mainButton?.let {
            if (isImmediately) {
                it.showImmediately()
            } else {
                it.show()
            }
        }
    }

    @JvmOverloads
    fun hide(isImmediately: Boolean = false) {
        mainButton?.let {
            if (isImmediately) {
                it.hideImmediately()
            } else {
                it.hide()
            }
        }
    }

//--------------------------------------------------------------------------------------------------
//  Event handlers
//--------------------------------------------------------------------------------------------------

    fun onFabClick(fabClickListener: OnFabClickListener) {
        this.fabClickListener = fabClickListener
    }

    fun onFabLongClick(fabLongClickListener: OnFabLongClickListener) {
        this.fabLongClickListener = fabLongClickListener
    }

//--------------------------------------------------------------------------------------------------
//  Saving & restoring state
//--------------------------------------------------------------------------------------------------

    public override fun onSaveInstanceState(): Parcelable? {
        val superState = super.onSaveInstanceState()
        val ss = SavedState(superState)
        ss.expanded = isExpanded
        ss.visibility = visibility
        return ss
    }

    public override fun onRestoreInstanceState(state: Parcelable) {
        if (state is SavedState) {
            val ss: SavedState = state

            isExpanded = ss.expanded
            setChildrenClickable(isExpanded)

            if (isExpanded) {
                mainButton?.setImageResource(expandedDrawableRes)
                addScrim()
                scrimView.background?.alpha = SCRIM_ALPHA
            } else {
                mainButton?.setImageResource(collapsedDrawableRes)
            }

            visibility = ss.visibility

            super.onRestoreInstanceState(ss.superState)
        } else {
            super.onRestoreInstanceState(state)
        }
    }

    class SavedState : BaseSavedState {
        var expanded: Boolean = false
        var visibility: Int = 0
        var downHidden: Boolean = false

        constructor(parcel: Parcelable?) : super(parcel)

        private constructor(parcel: Parcel) : super(parcel) {
            expanded = parcel.readInt().toBoolean()
            visibility = parcel.readInt()
            downHidden = parcel.readInt().toBoolean()
        }

        override fun writeToParcel(out: Parcel, flags: Int) {
            super.writeToParcel(out, flags)
            out.writeInt(expanded.toInt())
            out.writeInt(visibility)
            out.writeInt(downHidden.toInt())
        }

        companion object CREATOR : Parcelable.Creator<SavedState?> {
            override fun createFromParcel(parcel: Parcel): SavedState? = SavedState(parcel)

            override fun newArray(size: Int): Array<SavedState?> = arrayOfNulls(size)
        }
    }

    companion object {

        const val EXPAND_UP = 0
        const val EXPAND_DOWN = 1
        const val EXPAND_LEFT = 2
        const val EXPAND_RIGHT = 3

        const val LABELS_ON_LEFT_SIDE = 0
        const val LABELS_ON_RIGHT_SIDE = 1

        const val ANIMATION_DURATION = 300L
        private const val SCRIM_ALPHA = 140

        private val collapseInterpolator: Interpolator = DecelerateInterpolator(3F)
        private val expandInterpolator =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                OvershootInterpolator()
            } else {
                collapseInterpolator
            }
        private val alphaExpandInterpolator: Interpolator = DecelerateInterpolator()
    }
}