package com.olekdia.fam

fun interface OnFabVisibilityChangeListener {
    fun onFabVisibilityChange(isVisible: Boolean)
}