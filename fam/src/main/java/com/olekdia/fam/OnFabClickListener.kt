package com.olekdia.fam

fun interface OnFabClickListener {
    fun onFabClick(btnId: Int)
}