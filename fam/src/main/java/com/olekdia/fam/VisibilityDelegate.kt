package com.olekdia.fam

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.view.View
import com.olekdia.androidcommon.extensions.getInt

class VisibilityDelegate(private val fab: View) {

    private val showHideDuration: Long = fab.context
        .getInt(R.integer.show_hide_animation_duration)
        .toLong()

    private val showAnimListener: AnimatorListenerAdapter =
        object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                fabVisibilityChangeListener?.onFabVisibilityChange(isVisible = true)
            }
        }

    private val hideAnimListener: AnimatorListenerAdapter =
        object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                fab.visibility = View.GONE
                fabVisibilityChangeListener?.onFabVisibilityChange(isVisible = false)
            }
        }

    var fabVisibilityChangeListener: OnFabVisibilityChangeListener? = null

//--------------------------------------------------------------------------------------------------
//  Public methods
//--------------------------------------------------------------------------------------------------

    fun showImmediately() {
        fab.apply {
            animate().cancel()

            alpha = 1F
            scaleY = 1F
            scaleX = 1F
            translationY = 0F
            visibility = View.VISIBLE
        }
    }

    fun show() {
        if (fab.visibility != View.VISIBLE || fab.translationY != 0F) {
            fab.apply {
                animate().cancel()

                scaleX = 0F
                scaleY = 0F
                alpha = 0F
                translationY = 0F
                visibility = View.VISIBLE

                animate()
                    .scaleX(1F)
                    .scaleY(1F)
                    .alpha(1F)
                    .setDuration(showHideDuration)
                    .setListener(showAnimListener)
                    .start()
            }
        }
    }

    fun hide() {
        if (fab.visibility == View.VISIBLE) {
            fab.apply {
                animate().cancel()

                alpha = 1F
                scaleY = 1F
                scaleX = 1F

                animate()
                    .scaleX(0F)
                    .scaleY(0F)
                    .alpha(0F)
                    .setDuration(showHideDuration)
                    //.setInterpolator(FAST_OUT_SLOW_IN_INTERPOLATOR)
                    .setListener(hideAnimListener)
                    .start()
            }
        }
    }

    fun hideImmediately() {
        fab.visibility = View.GONE
    }

//    companion object {
//        val FAST_OUT_SLOW_IN_INTERPOLATOR: Interpolator = FastOutSlowInInterpolator()
//    }
}