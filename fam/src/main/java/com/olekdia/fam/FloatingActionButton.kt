package com.olekdia.fam

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.TypedArray
import android.os.Bundle
import android.os.Parcelable
import android.util.AttributeSet
import android.view.View
import android.widget.TextView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.stateful.ExtendableSavedState

class FloatingActionButton @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FloatingActionButton(context, attrs, defStyleAttr) {

    var title: String? = null
        set(title) {
            field = title
            labelView?.text = title
        }

    val labelView: TextView?
        get() = getTag(R.id.fab_label) as? TextView

    val isVisible: Boolean
        get() = visibility == View.VISIBLE

    var fabVisibilityChangeListener: OnFabVisibilityChangeListener?
        get() = visDelegate?.fabVisibilityChangeListener
        set(value) {
            visDelegate?.fabVisibilityChangeListener = value
        }

    private var visDelegate: VisibilityDelegate? = null

    init {
        val a: TypedArray = context.obtainStyledAttributes(
            attrs,
            R.styleable.FloatingActionButton,
            defStyleAttr,
            0
        )

        title = a.getString(R.styleable.FloatingActionButton_fab_title)
        visDelegate = VisibilityDelegate(this)

        a.recycle()
    }

    override fun show() {
        visDelegate?.show()
    }

    fun showImmediately() {
        visDelegate?.showImmediately()
    }

    override fun hide() {
        visDelegate?.hide()
    }

    @SuppressLint("RestrictedApi")
    fun hideImmediately() {
        visibility = View.GONE
    }

//--------------------------------------------------------------------------------------------------
//  Saving & restoring state
//--------------------------------------------------------------------------------------------------

    override fun onSaveInstanceState(): Parcelable? {
        val superState: Parcelable? = super.onSaveInstanceState()
        val data = Bundle().apply {
            putInt(VISIBILITY_TAG, visibility)
        }

        return ExtendableSavedState(superState).apply {
            extendableStates.put(VISIBILITY_TAG, data)
        }
    }

    @SuppressLint("RestrictedApi")
    override fun onRestoreInstanceState(state: Parcelable) {
        if (state !is ExtendableSavedState) {
            super.onRestoreInstanceState(state)
        } else {
            super.onRestoreInstanceState(state.superState)

            state.extendableStates[VISIBILITY_TAG]?.let {
                visibility = it.getInt(VISIBILITY_TAG)
            }
        }

        requestLayout()
    }

    companion object {
        private const val VISIBILITY_TAG = "visibility"
    }
}