package com.olekdia.fam

fun interface OnFabLongClickListener {
    fun onFabLongClick(btnId: Int)
}