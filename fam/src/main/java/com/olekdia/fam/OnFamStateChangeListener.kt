package com.olekdia.fam

fun interface OnFamStateChangeListener {
    fun onFamStateChange(isExpanded: Boolean)
}