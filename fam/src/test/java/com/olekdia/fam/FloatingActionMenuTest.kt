package com.olekdia.fam

import android.content.Context
import android.os.Build
import android.os.Looper.getMainLooper
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import com.olekdia.fam.test.R
import io.mockk.mockk
import io.mockk.mockkObject
import io.mockk.verify
import org.robolectric.Shadows.shadowOf
import org.robolectric.annotation.LooperMode

@LooperMode(LooperMode.Mode.PAUSED)
@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.O_MR1])
class FloatingActionMenuTest {

    private lateinit var context: Context
    private lateinit var inflater: LayoutInflater

    private lateinit var onFamStateChangeListener: OnFamStateChangeListener
    private lateinit var onFabClickListener: OnFabClickListener
    private lateinit var onFabLongClickListener: OnFabLongClickListener

    private lateinit var fam: FloatingActionsMenu
    private val numChildButtons: Int = 3

    @Before
    fun setup() {
        context = InstrumentationRegistry.getInstrumentation().context
        inflater = LayoutInflater.from(context)

        onFamStateChangeListener = mockk(relaxed = true, relaxUnitFun = true)
        onFabClickListener = mockk(relaxed = true, relaxUnitFun = true)
        onFabLongClickListener = mockk(relaxed = true, relaxUnitFun = true)

        fam = inflater.inflate(R.layout.test_fam, null) as FloatingActionsMenu
    }

    private fun waitUi() {
        shadowOf(getMainLooper()).idle()
    }

    private fun assertFabNotClickable() {
        for (i in 0 until fam.childCount / 2 - 1) { // -1 to exclude add button
            assertFalse(fam.getChildAt(i).isClickable)
        }
    }

    private fun assertFabClickable() {
        for (i in 0 until fam.childCount / 2 - 1) { // -1 to exclude add button
            assertTrue(fam.getChildAt(i).isClickable)
        }
    }

//--------------------------------------------------------------------------------------------------
//  Creation
//--------------------------------------------------------------------------------------------------

    @Test
    fun `create fam from xml - fam is correct`() {
        assertTrue(fam.isExpandable)
        assertNotNull(fam.mainButton)

        assertEquals(R.mipmap.icb_task, fam.expandedDrawableRes)
        assertEquals(R.mipmap.icf_add, fam.collapsedDrawableRes)

        assertEquals(10, fam.childCount)

        for (i in 0 until fam.childCount / 2) {
            assertTrue(fam.getChildAt(i) is FloatingActionButton)
        }
        for (i in fam.childCount / 2 until fam.childCount) {
            assertTrue(fam.getChildAt(i) is TextView)
        }

        assertEquals(
            context.getString(R.string.cat),
            (fam.getChildAt(5) as TextView).text
        )
        assertEquals(
            context.getString(R.string.act),
            (fam.getChildAt(6) as TextView).text
        )
        assertEquals(
            context.getString(R.string.reminder),
            (fam.getChildAt(7) as TextView).text
        )
        assertEquals(
            context.getString(R.string.estimation),
            (fam.getChildAt(8) as TextView).text
        )
        assertEquals(
            context.getString(R.string.task),
            (fam.getChildAt(9) as TextView).text
        )
    }

//--------------------------------------------------------------------------------------------------
//  Methods
//--------------------------------------------------------------------------------------------------

    @Test
    fun `setExpandedAddTitle, title is null - add button label is empty`() {
        assertEquals(10, fam.childCount)

        assertTrue(fam.getChildAt(4) is FloatingActionButton)
        assertTrue(fam.getChildAt(4) === fam.mainButton)

        assertTrue(fam.getChildAt(9) is TextView)
        assertEquals("Task", (fam.getChildAt(9) as TextView).text)

        fam.setExpandedMainTitle(null)

        assertEquals("", (fam.getChildAt(9) as TextView).text)
    }

    @Test
    fun `setExpandedAddTitle, title is empty - add button label is empty`() {
        assertEquals(10, fam.childCount)

        assertTrue(fam.getChildAt(4) is FloatingActionButton)
        assertTrue(fam.getChildAt(4) === fam.mainButton)

        assertTrue(fam.getChildAt(9) is TextView)
        assertEquals("Task", (fam.getChildAt(9) as TextView).text)

        fam.setExpandedMainTitle("")

        assertEquals("", (fam.getChildAt(9) as TextView).text)
    }

    @Test
    fun `setExpandedAddTitle, title is not empty - add button label changed`() {
        assertEquals(10, fam.childCount)

        assertTrue(fam.getChildAt(4) is FloatingActionButton)
        assertTrue(fam.getChildAt(4) === fam.mainButton)

        assertTrue(fam.getChildAt(9) is TextView)
        assertEquals("Task", (fam.getChildAt(9) as TextView).text)

        fam.setExpandedMainTitle("new label")

        assertEquals("new label", (fam.getChildAt(9) as TextView).text)
    }

    @Test
    fun `addButton - button added`() {
        val button: FloatingActionButton =
            inflater.inflate(R.layout.test_new_button_fab, null) as FloatingActionButton

        fam.addButton(button)

        assertEquals(12, fam.childCount)

        assertTrue(fam.getChildAt(4) is FloatingActionButton)
        assertTrue(fam.getChildAt(4) === button)

        assertTrue(fam.getChildAt(5) is FloatingActionButton)
        assertTrue(fam.getChildAt(5) === fam.mainButton)

        assertTrue(fam.getChildAt(10) is TextView)
        assertEquals("New", (fam.getChildAt(10) as TextView).text)

        assertTrue(fam.getChildAt(11) is TextView)
        assertEquals("Task", (fam.getChildAt(11) as TextView).text)
    }


    @Test
    fun `removeButton, not exist button - nothing changed`() {
        val button: FloatingActionButton =
            inflater.inflate(R.layout.test_new_button_fab, null) as FloatingActionButton

        fam.removeButton(button)

        assertEquals(10, fam.childCount)

        for (i in 0 until fam.childCount / 2) {
            assertTrue(fam.getChildAt(i) is FloatingActionButton)
        }
        for (i in fam.childCount / 2 until fam.childCount) {
            assertTrue(fam.getChildAt(i) is TextView)
        }

        assertEquals(
            context.getString(R.string.cat),
            (fam.getChildAt(5) as TextView).text
        )
        assertEquals(
            context.getString(R.string.act),
            (fam.getChildAt(6) as TextView).text
        )
        assertEquals(
            context.getString(R.string.reminder),
            (fam.getChildAt(7) as TextView).text
        )
        assertEquals(
            context.getString(R.string.estimation),
            (fam.getChildAt(8) as TextView).text
        )
        assertEquals(
            context.getString(R.string.task),
            (fam.getChildAt(9) as TextView).text
        )
    }

    @Test
    fun `removeButton, exist button - button removed`() {
        val button: FloatingActionButton = fam.getChildAt(0) as FloatingActionButton

        fam.removeButton(button)

        assertEquals(8, fam.childCount)

        for (i in 0 until fam.childCount / 2) {
            assertTrue(fam.getChildAt(i) is FloatingActionButton)
        }
        for (i in fam.childCount / 2 until fam.childCount) {
            assertTrue(fam.getChildAt(i) is TextView)
        }

        assertEquals(
            context.getString(R.string.act),
            (fam.getChildAt(4) as TextView).text
        )
        assertEquals(
            context.getString(R.string.reminder),
            (fam.getChildAt(5) as TextView).text
        )
        assertEquals(
            context.getString(R.string.estimation),
            (fam.getChildAt(6) as TextView).text
        )
        assertEquals(
            context.getString(R.string.task),
            (fam.getChildAt(7) as TextView).text
        )
    }

    @Test
    fun `hideButtons, empty indexes - nothing changed`() {
        fam.hideButtons()

        for (i in 0 until fam.childCount) {
            val view: View = fam.getChildAt(i)

            assertEquals(View.VISIBLE, view.visibility)
        }
    }

    @Test
    fun `hideButtons, single index - correct button and label are not visible`() {
        fam.hideButtons(0)

        for (i in 0..numChildButtons + 1) {
            val button = fam.getChildAt(i) as FloatingActionButton

            if (i == 0) {
                assertEquals(View.GONE, button.visibility)
                assertEquals(View.GONE, button.labelView!!.visibility)
            } else {
                assertEquals(View.VISIBLE, button.visibility)
                assertEquals(View.VISIBLE, button.labelView!!.visibility)
            }
        }
    }

    @Test
    fun `hideButtons, multi index - correct buttons and labels are not visible`() {
        fam.hideButtons(0, 2)

        for (i in 0..numChildButtons + 1) {
            val button: FloatingActionButton = fam.getChildAt(i) as FloatingActionButton

            if (i == 0 || i == 2) {
                assertEquals(View.GONE, button.visibility)
                assertEquals(View.GONE, button.labelView!!.visibility)
            } else {
                assertEquals(View.VISIBLE, button.visibility)
                assertEquals(View.VISIBLE, button.labelView!!.visibility)
            }
        }
    }

    @Test
    fun `hideButtons, single index, with hidden mainButton - mainButton hidden`() {
        fam.hide(isImmediately = true)
        assertEquals(View.GONE, fam.mainButton!!.visibility)

        fam.hideButtons(0)

        for (i in 0..numChildButtons) {
            val button: FloatingActionButton = fam.getChildAt(i) as FloatingActionButton

            if (i == 0) {
                assertEquals(View.GONE, button.visibility)
                assertEquals(View.GONE, button.labelView!!.visibility)
            } else {
                assertEquals(View.VISIBLE, button.visibility)
                assertEquals(View.VISIBLE, button.labelView!!.visibility)
            }
        }

        assertEquals(View.GONE, fam.mainButton!!.visibility)
    }


    @Test
    fun `collapse, isExpanded is false, immediately is false - nothing changed, listener not triggered`() {
        fam.famStateChangeListener = onFamStateChangeListener

        assertFalse(fam.isExpanded)
        assertFabNotClickable()

        fam.collapse(false)

        assertFalse(fam.isExpanded)
        assertFabNotClickable()

        verify(exactly = 0) { onFamStateChangeListener.onFamStateChange(any()) }
    }

    @Test
    fun `collapse, isExpanded is false, immediately is true - nothing changed, listener not triggered`() {
        fam.famStateChangeListener = onFamStateChangeListener

        assertFalse(fam.isExpanded)
        assertFabNotClickable()

        fam.collapse(true)

        assertFalse(fam.isExpanded)
        assertFabNotClickable()

        verify(exactly = 0) { onFamStateChangeListener.onFamStateChange(any()) }
    }

    @Test
    fun `collapse, isExpanded is true, immediately is false - isExpanded is false, buttons is not clickable, listener triggered`() {
        fam.famStateChangeListener = onFamStateChangeListener
        fam.expand(true)

        assertTrue(fam.isExpanded)
        assertFabClickable()

        fam.collapse(false)
        waitUi()

        assertFalse(fam.isExpanded)
        assertFabNotClickable()

        verify(exactly = 1) { onFamStateChangeListener.onFamStateChange(isExpanded = false) }
    }

    @Test
    fun `collapse, isExpanded is true, immediately is true - isExpanded is false, buttons is not clickable, listener triggered`() {
        fam.famStateChangeListener = onFamStateChangeListener
        fam.expand(true)

        assertTrue(fam.isExpanded)
        assertFabClickable()

        fam.collapse(true)
        waitUi()

        assertFalse(fam.isExpanded)
        assertFabNotClickable()

        verify(exactly = 1) { onFamStateChangeListener.onFamStateChange(isExpanded = false) }
    }


    @Test
    fun `toggle, isExpanded is false - expand triggered`() {
        assertFalse(fam.isExpanded)

        mockkObject(fam)
        fam.toggle()

        verify(exactly = 1) { fam.expand() }
    }

    @Test
    fun `toggle, isExpanded is true - collapse triggered`() {
        fam.expand(true)
        assertTrue(fam.isExpanded)

        mockkObject(fam)
        fam.toggle()

        verify(exactly = 1) { fam.collapse() }
    }


    @Test
    fun `expand, isExpanded is true, immediately is false - nothing changed, listener not triggered`() {
        fam.famStateChangeListener = onFamStateChangeListener
        fam.expand(true)
        verify(exactly = 1) { onFamStateChangeListener.onFamStateChange(isExpanded = true) }

        assertTrue(fam.isExpanded)
        assertFabClickable()

        fam.expand(false)

        assertTrue(fam.isExpanded)
        assertFabClickable()

        verify(exactly = 1) { onFamStateChangeListener.onFamStateChange(isExpanded = true) }
    }

    @Test
    fun `expand, isExpanded is true, immediately is true - nothing changed, listener not triggered`() {
        fam.famStateChangeListener = onFamStateChangeListener
        fam.expand(true)
        verify(exactly = 1) { onFamStateChangeListener.onFamStateChange(isExpanded = true) }

        assertTrue(fam.isExpanded)
        assertFabClickable()

        fam.expand(true)

        assertTrue(fam.isExpanded)
        assertFabClickable()

        verify(exactly = 1) { onFamStateChangeListener.onFamStateChange(isExpanded = true) }
    }

    @Test
    fun `expand, isExpanded is false, immediately is false - isExpanded is true, buttons are clickable, listener triggered`() {
        fam.famStateChangeListener = onFamStateChangeListener

        assertFalse(fam.isExpanded)
        assertFabNotClickable()

        fam.expand(false)

        assertTrue(fam.isExpanded)
        assertFabClickable()

        verify(exactly = 1) { onFamStateChangeListener.onFamStateChange(isExpanded = true) }
    }

    @Test
    fun `expand, isExpanded is false, immediately is true - isExpanded is true, buttons are clickable, listener triggered`() {
        fam.famStateChangeListener = onFamStateChangeListener

        assertFalse(fam.isExpanded)
        assertFabNotClickable()

        fam.expand(true)

        assertTrue(fam.isExpanded)
        assertFabClickable()

        verify(exactly = 1) { onFamStateChangeListener.onFamStateChange(isExpanded = true) }
    }


    @Test
    fun `setEnabled - addButton enable is valid`() {
        assertEquals(true, fam.mainButton!!.isEnabled)

        fam.isEnabled = false
        assertEquals(false, fam.mainButton!!.isEnabled)

        fam.isEnabled = true
        assertEquals(true, fam.mainButton!!.isEnabled)
    }


    @Test
    fun `show, addButton is visible, immediately is false - nothing changed`() {
        assertEquals(View.VISIBLE, fam.mainButton!!.visibility)

        fam.show(false)

        assertEquals(View.VISIBLE, fam.mainButton!!.visibility)
    }

    @Test
    fun `show, addButton is visible, immediately is true - nothing changed`() {
        assertEquals(View.VISIBLE, fam.mainButton!!.visibility)

        fam.show(true)

        assertEquals(View.VISIBLE, fam.mainButton!!.visibility)
    }

    @Test
    fun `show, addButton is not visible, immediately is false - addButton is visible`() {
        fam.hide(true)
        assertEquals(View.GONE, fam.mainButton!!.visibility)

        fam.show(false)

        assertEquals(View.VISIBLE, fam.mainButton!!.visibility)
    }

    @Test
    fun `show, addButton is not visible, immediately is true - addButton is visible`() {
        fam.hide(true)
        assertEquals(View.GONE, fam.mainButton!!.visibility)

        fam.show(true)

        assertEquals(View.VISIBLE, fam.mainButton!!.visibility)
    }


    @Test
    fun `hide, addButton is visible, immediately is false - button is not visible`() {
        assertEquals(View.VISIBLE, fam.mainButton!!.visibility)

        fam.hide(false)
        waitUi()

        assertEquals(View.GONE, fam.mainButton!!.visibility)
    }

    @Test
    fun `hide, addButton is visible, immediately is true - button is not visible`() {
        assertEquals(View.VISIBLE, fam.mainButton!!.visibility)

        fam.hide(true)

        assertEquals(View.GONE, fam.mainButton!!.visibility)
    }

    @Test
    fun `hide, addButton is not visible, immediately is false - nothing changed`() {
        fam.hide(true)
        assertEquals(View.GONE, fam.mainButton!!.visibility)

        fam.hide(false)


        assertEquals(View.GONE, fam.mainButton!!.visibility)
    }

    @Test
    fun `hide, addButton is not visible, immediately is true - nothing changed`() {
        fam.hide(true)
        assertEquals(View.GONE, fam.mainButton!!.visibility)

        fam.hide(true)

        assertEquals(View.GONE, fam.mainButton!!.visibility)
    }

//--------------------------------------------------------------------------------------------------
//  Listeners
//--------------------------------------------------------------------------------------------------

    @Test
    fun `OnFamStateChangeListener, fam is not expanded, click addButton - fam is expanded, listener triggered`() {
        fam.famStateChangeListener = onFamStateChangeListener

        assertFalse(fam.isExpanded)

        fam.mainButton!!.performClick()
        assertTrue(fam.isExpanded)

        verify(exactly = 1) { onFamStateChangeListener.onFamStateChange(isExpanded = true) }
    }

    @Test
    fun `OnFamStateChangeListener, fam is expanded, click addButton - fam is expanded, listener triggered`() {
        fam.famStateChangeListener = onFamStateChangeListener

        assertFalse(fam.isExpanded)
        fam.mainButton!!.performClick()
        assertTrue(fam.isExpanded)

        fam.mainButton!!.performClick()
        assertFalse(fam.isExpanded)

        verify(exactly = 1) { onFamStateChangeListener.onFamStateChange(isExpanded = false) }
    }


    @Test
    fun `OnFabClickListener, fam is not expanded, perform button click - listener not triggered`() {
        fam.fabClickListener = onFabClickListener

        assertFalse(fam.isExpanded)

        fam.getChildAt(0).performClick()

        verify(exactly = 0) { onFabClickListener.onFabClick(R.id.fab_new_category) }
    }

    @Test
    fun `OnFabClickListener, fam is expanded, perform button click - listener triggered`() {
        fam.fabClickListener = onFabClickListener

        assertFalse(fam.isExpanded)
        fam.mainButton!!.performClick()
        assertTrue(fam.isExpanded)

        fam.getChildAt(0).performClick()

        verify(exactly = 1) { onFabClickListener.onFabClick(R.id.fab_new_category) }
    }


    @Test
    fun `OnFabLongClickListener, fam is not expanded, perform button click - listener not triggered`() {
        fam.fabClickListener = onFabClickListener

        assertFalse(fam.isExpanded)

        fam.getChildAt(0).performLongClick()

        verify(exactly = 0) { onFabLongClickListener.onFabLongClick(R.id.fab_new_category) }
    }

    @Test
    fun `OnFabLongClickListener, fam is expanded, perform button click - long listener triggered, click listener not`() {
        fam.fabLongClickListener = onFabLongClickListener
        fam.fabClickListener = onFabClickListener

        assertFalse(fam.isExpanded)
        fam.mainButton!!.performClick()
        assertTrue(fam.isExpanded)

        fam.getChildAt(0).performLongClick()

        verify(exactly = 1) { onFabLongClickListener.onFabLongClick(R.id.fab_new_category) }
        verify(exactly = 0) { onFabClickListener.onFabClick(R.id.fab_new_category) }
    }
}