An implementation of floating action menu https://material.io/components/buttons-floating-action-button

Gradle:

```implementation 'com.olekdia:fam:3.4.1'```

![Sample of float action menu](img/sample.gif)